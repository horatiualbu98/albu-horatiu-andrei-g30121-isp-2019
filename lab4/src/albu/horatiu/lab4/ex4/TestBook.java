package albu.horatiu.lab4.ex4;

import albu.horatiu.lab4.ex2.Author;

public class TestBook {
    public static void main(String[] args) {
        Author a1=new Author("ada@gmail.com","Sasuke",'m');
        Author a2=new Author("faead@yahoo.com","Gigi",'f');

        Book b1 = new Book("afafa", "afawda", 'm', "Horica", 50,new Author[]{a2});
        Book b2 = new Book("asdafafd", "horyalbu98@gmail.com", 'm', "vasca", 30,new Author[]{a1});
        b2.toString();
        b1.toString();
    }
}
