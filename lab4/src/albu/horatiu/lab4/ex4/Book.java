package albu.horatiu.lab4.ex4;


import albu.horatiu.lab4.ex2.Author;

public class Book extends Author {


    private static String name;
    private double price;
    private Author[] author;
    private int qtyInStock = 0;

    public Book(String name, String email, char gender, String name1, double price, Author[] authors) {
        super(name, email, gender);
        this.name = name1;
        this.price = price;
        this.author = authors;
    }

    public Book(String name, String email, char gender, String name1, double price, Author[] authors, int qtyInStock) {
        super(name, email, gender);
        this.name = name1;
        this.price = price;
        this.author = authors;
        this.qtyInStock = qtyInStock;
    }
@Override
    public String getName() {
        return name;
    }

    public Author[] getAuthors() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    public String toString() {
        return ("book-" + getName() + " by " + getAuthors());

    }


    public void printAuthors() {
        for (int i = 0; i <= author.length; i++) System.out.println(author[i].getName());
    }
}