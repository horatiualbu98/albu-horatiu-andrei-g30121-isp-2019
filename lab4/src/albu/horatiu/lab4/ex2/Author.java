package albu.horatiu.lab4.ex2;

public class Author {
    private String name;
    private String email;
    private char gender;

    public Author(String email, String name, char gender) {
        this.name = name;
        this.email = email;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;

    }

    public void setEmail(String email) {
        this.email = email;

    }

    public char getGender() {
        return gender;
    }


    public String toString(String n, String e, char g) {

        return ("Author-" + n + " " + g + " at " + e);
    }}


