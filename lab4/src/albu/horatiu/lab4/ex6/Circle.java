package albu.horatiu.lab4.ex6;

import static java.lang.Math.pow;

import albu.horatiu.lab4.ex6.Shape;

public class Circle extends Shape {
    private double radius;
    private String color;
    private static final float PI = 3.1415f;

    public Circle() {
        this.radius = 1.0;
        this.color = "red";
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return PI * pow(radius, 2);
    }
@Override
    public String toString() {
        System.out.println("A Circle with radius " +radius+ " which is a subclass of "+ super.toString());
        return null;
    }
}


