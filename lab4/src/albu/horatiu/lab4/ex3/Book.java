package albu.horatiu.lab4.ex3;

import albu.horatiu.lab4.ex2.Author;

public class Book extends Author {


    private static String name;
    private double price;
    private String author;
    private int qtyInStock = 0;

    public Book(String name, String email, char gender, String name1, double price, String author) {
        super(name, email, gender);
        this.name = name1;
        this.price = price;
        this.author = author;
    }

    public Book(String name, String email, char gender, String name1, double price, String author, int qtyInStock) {
        super(name, email, gender);
        this.name = name1;
        this.price = price;
        this.author = author;
        this.qtyInStock = qtyInStock;
    }


    public String getNume() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    public void tooString() {
        System.out.println("book-" + getNume() + " by " + super.toString("Albu Horatiu", "horyalbu98@gmail.com", 'm'));

    }
}