package albu.horatiu.lab4.ex1;

public class TestCircle {

    public static void main(String[] args) {
        Circle c1 = new Circle();
        System.out.println("Raza pentru primul cerc este:  " + c1.getRadius() + "\n" + "Aria pentru primul cerc este: " + c1.getArea() + "\n");
        Circle c2 = new Circle(3);
        System.out.println("Raza pentru al doilea cerc este:  " + c2.getRadius() + "\n" + "Aria pentru al doilea cerc este: " + c2.getArea() + "\n");

    }
}
