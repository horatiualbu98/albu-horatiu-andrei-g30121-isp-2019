package albu.horatiu.lab4.ex1;

import static java.lang.Math.pow;

public class Circle {
    private double radius;
    private String color;
    private static final float PI = 3.1415f;


    public Circle() {
        this.radius = 1.0;
        this.color = "red";
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return PI * pow(radius, 2);
    }
//
//    public double getAreaCylinder()
//    { return  2 * PI * radius *(radius + super(height));
//
//    }
}

