package albu.horatiu.lab4.ex5;

import albu.horatiu.lab4.ex1.Circle;


public class Cylinder extends Circle {
    private double height;
    private static final float PI = 3.1415f;

    public Cylinder() {
        this.height = 1.0;
    }

    public Cylinder(double radius) {
        super(radius);
    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume() {
        return height * super.getArea();

    }
    public double getAreaCyilinder(){

        return 2*super.getArea()+ 2*PI*(getRadius()*height);
    }
}