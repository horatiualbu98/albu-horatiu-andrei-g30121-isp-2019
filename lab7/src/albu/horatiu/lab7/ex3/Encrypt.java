package albu.horatiu.lab7.ex3;

import java.io.*;
import java.util.Scanner;

public class Encrypt {
    public static void action(FileReader file, String fileName) throws IOException {
        BufferedReader reader = new BufferedReader(file);
        String input = reader.readLine();
        System.out.println(input);

        char[] stringToCharArr = input.toCharArray();
        String outString = "";

        for (char crt : stringToCharArr) {
            outString += String.valueOf(++crt);
        }

        System.out.println(outString);

        PrintWriter out = new PrintWriter("dataIn/" + fileName + ".enc");
        out.println(outString);
        out.close();
    }
}

class test {
    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);

        System.out.println("Introduceti numele fisierului pt encriptat:");
        String fileName = scan.nextLine();

        FileReader file = new FileReader("dataIn/" + fileName + ".dec");
        Encrypt.action(file, fileName);

        System.out.println("Introduceti numele fisierului pt decriptat:");
        fileName = scan.nextLine();

        FileReader file2 = new FileReader("dataIn/" + fileName + ".enc");
        Decrypt.action(file2, fileName);
    }
}