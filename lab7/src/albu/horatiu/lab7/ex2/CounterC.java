package albu.horatiu.lab7.ex2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CounterC {
    public static void main(String[] args) {
        Path file = Paths.get("D:\\Facultate\\An2\\Sem2\\ISP\\albu-horatiu-andrei-g30121-isp-2019\\lab7\\src\\albu\\horatiu\\lab7\\ex2\\ex2.txt");
        CounterC cl1 = new CounterC(file, 'm');
        System.out.println(cl1.count());
    }

    private Path file;
    private char lookFor;

    CounterC(Path file, char lookFor) {
        this.file = file;
        this.lookFor = lookFor;
    }

    private int count() {
        int count = 0;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(Files.newInputStream(file)))) {
            String line = null;
            while ((line = br.readLine()) != null) {
                for (int i = 0; i < line.length(); i++) {
                    if (line.charAt(i) == lookFor) {
                        count++;
                    }
                }
            }
        } catch (IOException x) {
            System.err.println(x);
        }
        return count;
    }
}
