package albu.horatiu.lab7.ex1;

public class CoffeTest {
    public static void main(String[] args) {
        CofeeMaker mk = new CofeeMaker();
        CofeeDrinker d = new CofeeDrinker();

        for (int i = 0; i < 15; i++) {
            Cofee c = mk.makeCofee();
            try {
                d.drinkCofee(c);
            } catch (TemperatureException e) {
                System.out.println("Exception:" + e.getMessage() + " temp=" + e.getTemp());
            } catch (ConcentrationException e) {
                System.out.println("Exception:" + e.getMessage() + " conc=" + e.getConc());
            } catch (NumberException e) {
                System.out.println("Exception: " + e.getMessage() + " numar= " + e.getNumb());
            } finally {
                System.out.println("Throw the cofee cup.\n");
            }
        }
    }
}//.class

class CofeeMaker {
    Cofee makeCofee() {
        System.out.println("Make a coffe");
        int t = (int) (Math.random() * 100);
        int c = (int) (Math.random() * 100);
        int a = (int) (Math.random() * 100);
        Cofee cofee = new Cofee(t, c, a);
        return cofee;
    }

}//.class

class Cofee {
    private int temp;
    private int conc;
    public static int a = 0;

    Cofee(int t, int c, int a) {
        temp = t;
        conc = c;
        this.a = a;
        this.a++;
    }

    int getTemp() {
        return temp;
    }

    int getConc() {
        return conc;
    }

    public String toString() {
        return "[cofee temperature=" + temp + ":concentration=" + conc + "]";
    }
}//.class

class CofeeDrinker {
    void drinkCofee(Cofee c) throws TemperatureException, ConcentrationException, NumberException {
        if (c.getTemp() > 60)
            throw new TemperatureException(c.getTemp(), "Cofee is to hot!");
        if (c.getConc() > 50)
            throw new ConcentrationException(c.getConc(), "Cofee concentration to high!");
        if (Cofee.a > 50)
            throw new NumberException(Cofee.a, " Number is too high");
        System.out.println("Drink cofee:" + c);
    }
}//.class

class NumberException extends Exception {
    int a;

    public NumberException(int a, String msg) {
        super(msg);
        this.a = a;
    }


    int getNumb() {
        return a;
    }

}

class TemperatureException extends Exception {
    int t;

    public TemperatureException(int t, String msg) {
        super(msg);
        this.t = t;
    }

    int getTemp() {
        return t;
    }
}//.class


class ConcentrationException extends Exception {
    int c;

    public ConcentrationException(int c, String msg) {
        super(msg);
        this.c = c;
    }

    int getConc() {
        return c;
    }
}//.class