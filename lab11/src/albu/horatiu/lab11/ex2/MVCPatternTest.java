package albu.horatiu.lab11.ex2;


import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MVCPatternTest extends JFrame {

    static List<ProductController> controller = new ArrayList<ProductController>();

    public static void addProduct() {
        Product product = new Product();
        ProductView view = new ProductView();
        Scanner input = new Scanner(System.in);
        System.out.println("Introduceti numele: ");
        String nume = input.nextLine();
        product.setName(nume);
        System.out.println("Introduceti cantitatea");
        int cant= input.nextInt();
        product.setQuantity(cant);
        System.out.println("Introduceti Pretul");
        int pret=input.nextInt();
        product.setPrice(pret);
        controller.add(new ProductController(product, view));
    }

    public static void removeProduct(String nume) {
        List<ProductController> ControllersRemoved = new ArrayList<ProductController>();
        for (ProductController cont :
                controller) {
            if (cont.getProductName().equals(nume) == false) {
                ControllersRemoved.add(cont);
            }
        }
        controller.removeAll(controller);
        controller = ControllersRemoved;
    }

    public static void changeProductQty(String nume, int qty) {
        for (ProductController cont :
                controller) {
            if (cont.getProductName().equals(nume)) {
                cont.setProductQty(qty);
            }
        }
    }

    public static void main(String[] args) {
        for (int i = 0; i < 3; i++) addProduct();

        for (ProductController cont :
                controller) {
            cont.updateView();
        }

        removeProduct("aspirator");

        System.out.println("removing aspirator");
        for (ProductController cont :
                controller) {
            cont.updateView();
        }
        System.out.println("aspirator removed");

        for (int i = 0; i < 3; i++) addProduct();
        for (ProductController cont :
                controller) {
            cont.updateView();
        }


        System.out.println("Setting qty");
        changeProductQty("aspirator", 100000);
        for (ProductController cont :
                controller) {
            cont.updateView();
        }
    }

}
