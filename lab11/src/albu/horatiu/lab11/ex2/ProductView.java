package albu.horatiu.lab11.ex2;

public class ProductView {
    public void printProductDetails(String productName, int price, int qty) {
        System.out.println("Product:");
        System.out.println("- name: " + productName);
        System.out.println("- price: " + price);
        System.out.println("- quantity: " + qty);
    }
}
