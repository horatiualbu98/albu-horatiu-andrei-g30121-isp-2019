package albu.horatiu.lab11.ex1;

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;

class ObservedTemperatureSensor extends Observable implements Runnable {

    Random r = new Random();
    private int watchedValue = 0;

    public ObservedTemperatureSensor(int value) {
        watchedValue = value;
    }

    public synchronized void setWatchedValue() {
        int val = r.nextInt(100 + 20) + 20;
        if (val != watchedValue) {
            ObservableDemo.setTemp(val);
            watchedValue = val;
            setChanged();
            notifyObservers(val);
        }
    }

    @Override
    public void run() {
        Thread t = Thread.currentThread();
        while (true) {
            setWatchedValue();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class ObservableDemo extends JFrame implements Observer {

    static JTextField temp;
    static JTextArea observerOutput;
    static JLabel currentTemp;
    private static StringBuilder text = new StringBuilder("Observer: no observed event.\n");

    public ObservableDemo() {
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 400);

        temp = new JTextField("Current temperature: 0");
        temp.setBounds(10, 10, 365, 20);
        add(temp);

        currentTemp = new JLabel("Observer dialog:");
        currentTemp.setBounds(10, 30, 300, 20);
        add(currentTemp);

        observerOutput = new JTextArea();
        observerOutput.setBounds(10, 50, 365, 250);
        add(observerOutput);

        setVisible(true);
    }

    public static void setTemp(int tempToSet) {
        text.append("Observer: Value changed to: "+String.valueOf(tempToSet));
        text.append("\n");
        temp.setText("Current temperature: " + tempToSet);
        observerOutput.setText(text.toString());
    }

    public static void main(String[] args) throws InterruptedException {
        ObservedTemperatureSensor sensor = new ObservedTemperatureSensor(0);
        ObservableDemo watcher = new ObservableDemo();
        sensor.addObserver(watcher);
        Thread thread = new Thread(sensor, "thread1");
        thread.start();
    }

    public void update(Observable obj, Object arg) {
        System.out.println("Update called with Arguments: " + arg);
    }
}