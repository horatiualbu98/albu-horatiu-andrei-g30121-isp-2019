package albu.horatiu.lab8.ex2;

import java.util.Scanner;

public class Controler {
    Scanner in = new Scanner(System.in);
    int V= in.nextInt();
    Senzor s;
    Compresor c;

    Controler(Senzor s, Compresor c) {
        this.s = s;
        this.c = c;
    }

    void control() {
        if (s.valoare > V)
            c.pornesteCompresor();
        else
            c.opresteCompresor();
    }
}