package albu.horatiu.lab5.ex2;

public class Test {
    public static void main(String[] args){
        RealImage rimg = new RealImage("Fisier");
        rimg.display();
        ProxyImage pimg = new ProxyImage("PFisier");
        pimg.display();
        Image test = new RealImage("test");
        test.display();

        ProxyImage test2 = new ProxyImage("test2");
        test2.display();
        test2.RotatedImage();
        test2 = new ProxyImage("test2",1);
        test2.display();
    }
}
