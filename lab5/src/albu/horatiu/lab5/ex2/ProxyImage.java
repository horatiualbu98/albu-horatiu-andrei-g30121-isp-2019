package albu.horatiu.lab5.ex2;

class ProxyImage implements Image {

    private RealImage realImage;
    private String fileName;
    private int arg = 0;

    public ProxyImage(String fileName) {
        this.fileName = fileName;
    }

    public ProxyImage(String fileName, int arg) {
        this.fileName = fileName;
        this.arg = arg;
    }

    @Override
    public void display() {
        if (arg == 0) {
            if (realImage == null) {
                realImage = new RealImage(fileName);
            }
            realImage.display();
        } else RotatedImage();
    }


    public void RotatedImage() {
        if (arg == 1) {
            if (realImage == null) {
                realImage = new RealImage(fileName);
            }
            realImage.RotatedImage();
        } else display();
    }
}
