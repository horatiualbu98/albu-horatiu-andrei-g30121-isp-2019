package albu.horatiu.lab5.ex3;

import java.util.concurrent.TimeUnit;

public class Controller extends Sensor {

    public void control() throws InterruptedException {
        int count = 0;
        while (count < 20) {
            TimeUnit.SECONDS.sleep(1);
            System.out.println("TempSensor:" + TemperatureSensor.getTempSensor() + " si LightSensor:" + LightSensor.getLightSensor());
            count++;
        }
    }


}
