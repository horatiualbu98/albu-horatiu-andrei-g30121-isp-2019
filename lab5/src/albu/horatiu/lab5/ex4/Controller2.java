package albu.horatiu.lab5.ex4;

import albu.horatiu.lab5.ex3.LightSensor;
import albu.horatiu.lab5.ex3.TemperatureSensor;
import albu.horatiu.lab5.ex3.Sensor;

import java.util.concurrent.TimeUnit;

public class Controller2 extends Sensor {
    private static Controller2 controller2;

    public Controller2() {
    }


    public void control2() throws InterruptedException {
        int count = 0;
        while (count < 20) {
            TimeUnit.SECONDS.sleep(1);
            System.out.println("TempSensor:" + TemperatureSensor.getTempSensor() + " si LightSensor:" + LightSensor.getLightSensor());
            count++;
        }
    }

    public static Controller2 getController2() {
        if (controller2 == null) {
            controller2 = new Controller2();
        }
        return controller2;
    }

}

