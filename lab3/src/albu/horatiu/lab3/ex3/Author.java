package albu.horatiu.lab3.ex3;

public class Author {
    private String nume;
    private String email;
    private char gender;

    public Author(String nume, String email, char gender) {
        this.nume = nume;
        this.email = email;
        this.gender = gender;
    }

    public String getName() {
        return nume;

    }

    public String getEmail() {
        return email;


    }

    public char getGender() {
        return gender;

    }

    public void setEmail(String email) {
        this.email = email;

    }

    public void tooString() {
        System.out.println(this.nume + " (" + this.gender + ") at " + this.email);
    }
}