package albu.horatiu.lab3.ex4;

public class TestPoint {
    public static void main(String[] args) {
        MyPoint p1 = new MyPoint();
        MyPoint p2 = new MyPoint(2, 3);
        MyPoint p3 = new MyPoint(29, 30);
        p1.tooString();
        p2.tooString();
        p3.tooString();
        p1.setX(2);
        p1.setY(5);
        System.out.println(p1.getX());
        System.out.println(p1.getY());
        System.out.println("Distanta de la puntcul p3 la p2 este: "+p3.distance(p2));



    }
}
