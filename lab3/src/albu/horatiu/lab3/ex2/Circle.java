package albu.horatiu.lab3.ex2;

public class Circle {
    private double radius;
    private String color;

    public Circle() {
        this.radius = 5.0;
        this.color = "blue";
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle(double radius, String color) {
        this.radius = radius;
        this.color = color;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return 2 * Math.PI * radius;
    }
}
