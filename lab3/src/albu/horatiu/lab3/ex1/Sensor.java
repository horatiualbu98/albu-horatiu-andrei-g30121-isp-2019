package albu.horatiu.lab3.ex1;

public class Sensor {
    public int value;

    public Sensor() {
        this.value = -1;
    }

    public void change(int k) {
        this.value = k;
    }

    public int tooString() {
        return this.value;
    }
}
