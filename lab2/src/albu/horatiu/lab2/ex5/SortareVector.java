package albu.horatiu.lab2.ex5;

public class SortareVector {
    public static void main(String[] args) {
        int[] array = {1, 3, 1, 2, 4, 5, 6, 1, 2, 3, 10};

        int temp;
        for (int i = 0; i < array.length - 1; i++)
            for (int j = 0; j < array.length - i - 1; j++)
                if (array[j] > array[j + 1]) {
                    temp = array[j + 1];
                    array[j + 1] = array[j];
                    array[j] = temp;
                }

        System.out.println("Numerele ordonate sunt:");

        for (int i = 0; i < array.length; i++)
            System.out.print(" " + array[i]);

    }
}

