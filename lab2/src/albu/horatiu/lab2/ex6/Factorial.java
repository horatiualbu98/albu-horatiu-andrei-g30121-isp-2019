package albu.horatiu.lab2.ex6;

import java.util.Scanner;

public class Factorial {
    static int factorial(int n) {
        if (n == 0)
            return 1;
        else
            return (n * factorial(n - 1));
    }

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        System.out.println("Introduceti numarul");
        int number = in.nextInt();
        int i, fact = 1;
        fact = factorial(number);
        System.out.println(+number + " factorial este " + fact);
    }
}

