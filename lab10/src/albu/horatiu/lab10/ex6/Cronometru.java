package albu.horatiu.lab10.ex6;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.io.*;
import java.security.CryptoPrimitive;
import java.util.*;
import java.util.HashMap;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class Cronometru extends JFrame {

    JLabel timp;
    static JTextField timp1;
    JButton start, reset;
    private boolean hasStarted = false;

    public Cronometru() {
        setTitle("Cronometru");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(500, 500);
        setVisible(true);

    }

    void init() {
        this.setLayout(null);
        timp = new JLabel("Timp: ");
        timp.setBounds(30, 30, 100, 100);

        timp1 = new JTextField("0:0:0:000");
        timp1.setBounds(120, 70, 100, 20);

        start = new JButton("Start/Stop");
        start.setBounds(50, 120, 150, 20);

        reset = new JButton("Reset");
        reset.setBounds(250, 120, 150, 20);

        start.addActionListener(new TratareButoane());


        add(timp);
        add(timp1);
        add(start);
        add(reset);


    }

    public static void afisare(int msec, int sec, int min, int ora) {
        timp1.setText(ora + ":" + min + ":" + sec + ":" + msec);

    }


    public static void main(String[] args) {
        Cronometru c = new Cronometru();


    }

    class TratareButoane implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            RUN r = new RUN();

            synchronized (r) {
                if (((JButton) e.getSource()).getText().equals("Start/Stop")) {
                    r.setRunning(true);
                    if (hasStarted == false) {
                        r.start();
                        hasStarted = true;
                    }
                    notify();
                }


            }
            synchronized (r) {
                if (((JButton) e.getSource()).getText().equals("Start/Stop") && hasStarted) {
                    r.setRunning(false);
                    try {
                        this.wait();
                    } catch (InterruptedException f) {
                        System.out.println("nu merge");
                    }
                }
            }


            if (((JButton) e.getSource()).getText().equals("Reset")) {

                r.reset();
            }


        }

    }

}


class RUN extends Thread {
    int ms = 0;
    int sec = 0;
    int min = 0;
    int ora = 0;

    RUN() {
    }

    boolean running = false;

    public void setRunning(boolean running) {
        this.running = running;
    }

    public void reset() {
        ms = 0;
        sec = 0;
        min = 0;
        ora = 0;
    }

    public void run() {
        while (running) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ms++;
            if (ms == 1000) {
                ms = 0;
                sec++;
            }
            if (sec == 60) {
                sec = 0;
                min++;
            }
            if (min == 60) {
                min = 0;
                ora++;
            }

            Cronometru.afisare(ms, sec, min, ora);

        }


    }
}









