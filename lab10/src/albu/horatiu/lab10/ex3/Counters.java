package albu.horatiu.lab10.ex3;


public class Counters extends Thread {

    String n;
    Thread t;

    Counters(String n, Thread t) {
        this.n = n;
        this.t = t;
    }

    public void run() {
        System.out.println("Firul " + n + " executa operatie.");
        for (int i = 0; i < 101; i++) {
            System.out.println(getName() + " i = " + i);
        }

        try {
            if (t != null) t.join();
            Thread.sleep(300);
            System.out.println("Firul " + n + " a terminat operatia.");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void run2() {
        System.out.println("Firul " + n + " executa operatie.");
        for (int j =101; j < 201; j++) {
            System.out.println(getName() + " i = " + j);
        }

        try {
            if (t != null) t.join();
            Thread.sleep(300);
            System.out.println("Firul " + n + " a terminat operatia.");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        Counters c1 = new Counters("Counter1", null);
        Counters c2 = new Counters("counter2", c1);

        c1.run();
        c2.run2();

    }
}

