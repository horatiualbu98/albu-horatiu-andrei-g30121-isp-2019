package albu.horatiu.lab9.ex2;

import java.awt.FlowLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.util.*;

public class ButtonCounter extends JFrame {

    JLabel counter;
    JTextField tcounter;
    JButton Count;
    private int count=0 ;

    ButtonCounter() {

        setTitle("Counter");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200, 250);
        setVisible(true);
    }


    public void init() {

        this.setLayout(null);
        int width = 180;
        int height = 20;

        counter = new JLabel("Counter");
        counter.setBounds(10, 50, width, height);

        tcounter = new JTextField("0");
        tcounter.setBounds(70, 50, width, height);

        Count = new JButton("Start Counting");
        Count.setBounds(10, 150, width, height);

        Count.addActionListener(new TratareButonCount());

        add(counter);
        add(tcounter);
        add(Count);
    }

    public static void main(String[] args) {
        new ButtonCounter();
    }
    class TratareButonCount implements ActionListener {

        public void actionPerformed(ActionEvent evt) {
            count++;                     // Incrase the counter value
            tcounter.setText(count + ""); // Display on the TextField
            // setText() takes a String
        }

    }

}

