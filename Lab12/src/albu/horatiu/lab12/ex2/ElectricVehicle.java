package albu.horatiu.lab12.ex2;

import albu.horatiu.lab12.ex1.Vehicle;

public class ElectricVehicle extends Vehicle {
    public ElectricVehicle(String type, int length) {
        super(type, length);
    }
}
