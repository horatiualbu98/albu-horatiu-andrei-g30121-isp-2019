package albu.horatiu.lab12.ex2;
public class BatteryException extends Exception {

    BatteryException(String msg){
        super(msg);
    }

}