package albu.horatiu.lab6.ex3;

public class TestBank2 {
    public static void main(String[] args) {
        Bank2 bank2 = new Bank2();

        bank2.addAccount("Moco", 5000);
        bank2.addAccount("adsafa", 10000);

        bank2.printAccounts();
        bank2.printAccounts(6000, 25000);
    }
}