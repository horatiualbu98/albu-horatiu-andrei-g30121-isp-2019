package albu.horatiu.lab6.ex3;

public class BankAccount2 implements Comparable<BankAccount2> {

    private String owner;
    private double balance;

    public BankAccount2(String owner, double balance) {
        this.balance = balance;
        this.owner = owner;
    }

    public void withdraw(double amount) {
        this.balance -= amount;
    }

    public void deposit(double amount) {
        this.balance += amount;
    }

    public double getBalance() {
        return balance;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String toString() {
        return this.owner + " " + this.balance;
    }

    @Override
    public int compareTo(BankAccount2 acc) {
        if (this.getBalance() > acc.getBalance()) return 0;
        else if (this.getBalance() > acc.getBalance()) return 1;
        else return -1;
    }
}