package albu.horatiu.lab6.ex2;

import java.util.*;

import albu.horatiu.lab6.ex1.BankAccount;

import java.util.ArrayList;

public class Bank extends BankAccount {
    private ArrayList<BankAccount> accounts = new ArrayList<>();

    public void addAccount(String owner, double balance) {
        BankAccount account = new BankAccount(owner, balance);
        accounts.add(account);
    }

    public void printAccounts() {
        for (int i = 0; i < accounts.size(); i++) {
            System.out.println("Owner :" + accounts.get(i).getOwner() + " Balance " + accounts.get(i).getBalance());
        }
    }

    public void printAccounts(double minValue, double maxValue) {
        for (int i = 0; i < accounts.size(); i++) {
            if (accounts.get(i).getBalance() > minValue && accounts.get(i).getBalance() < maxValue)
                System.out.println("Owner :" + accounts.get(i).getOwner() + " Balance " + accounts.get(i).getBalance());
        }
    }
    public ArrayList<BankAccount> getAllAccounts() {
        return accounts;
    }
}
