package albu.horatiu.lab6.ex2;

import java.util.Collections;

import albu.horatiu.lab6.ex1.BankAccount;

import java.util.Comparator;


public class TestBank {
    public static void main(String[] args) {
        Bank b1 = new Bank();
        b1.addAccount("Gigel", 6000);
        b1.addAccount("Costel", 30000);
        b1.addAccount("Cornel",1);
        b1.printAccounts();
        b1.printAccounts(500, 1000000);

        Collections.sort(b1.getAllAccounts(), new Comparator<BankAccount>() {
            public int compare(BankAccount a1, BankAccount a2) {
                return String.valueOf(a1.getOwner()).compareTo(a2.getOwner());
            }
        });

        for (int i = 0; i < b1.getAllAccounts().size(); i++) {
            System.out.println("Owner's name:" + b1.getAllAccounts().get(i).getOwner() + " with a balance of:" + b1.getAllAccounts().get(i).getBalance());
        }
    }

}

