package albu.horatiu.lab6.ex1;

public class BankAccount {

    private String owner;
    private double balance;

    public BankAccount() {
    }

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }


    public void withdraw(double amount) {
        this.balance -= amount;
    }

    public void depozit(double amount) {
        this.balance += amount;
    }

    public double getBalance() {
        return balance;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
@Override
    public boolean equals(Object obj) {
        if (obj instanceof BankAccount) {
            BankAccount b = (BankAccount) obj;
            return balance == b.balance && b.owner.equals(owner);
        }
        return false;
    }
@Override
    public int hashCode() {
        return (int) balance + owner.hashCode();
    }
}

