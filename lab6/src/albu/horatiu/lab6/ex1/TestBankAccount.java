package albu.horatiu.lab6.ex1;

public class TestBankAccount {
    public static void main(String[] args) {
        BankAccount b1 = new BankAccount("Gigel", 5000);
        BankAccount b2 = new BankAccount("Gigel", 5000);
        BankAccount b3 = new BankAccount("Coco", 2000);

        if (b1 == b2) {
            System.out.println("b1 = b2");
        }
        if (b1.equals(b2)) {
            System.out.println("b1 equals b2");
        }
    }
}

